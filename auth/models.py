# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.template import loader
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.urlresolvers import reverse


class UserProfile(AbstractUser):
    biografia = models.TextField('Biografia', max_length=250, blank=True)
    foto = models.ImageField(
        'Foto', 
        upload_to='profile/', 
        null=True,
        blank=True)

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = '%ss' % verbose_name

    def get_absolute_url(self):
        return reverse('widgets.views.get_posts_por_usuario', args=[self.id])

    def save(self, **kwargs):
        senha = None
        if not self.password:
            senha = UserProfile.objects.make_random_password()
            self.set_password(senha)
        super(UserProfile, self).save(**kwargs)

        if senha:
            current_site = Site.objects.get_current()
            c = {
                'site_name': current_site.name,
                'domain': current_site.domain,
                'username': self.username,
                'password': senha,
            }
            subject = loader.render_to_string('registration/new_user_subject.txt', c)
            email = loader.render_to_string('registration/new_user_email.html', c)
            send_mail(subject, email, None, [self.email])