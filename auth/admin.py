from django.contrib import admin
from auth.models import *
from auth.forms import *
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext as _
from django.contrib.auth.models import Group


class UserProfileAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', )}),
        (_('Profile'), {'fields': ('foto', 'biografia')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'groups')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {'fields': ('username', )}),
        (_('Profile'), {'fields': ('foto', 'biografia')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'groups')}),
    )
    form = UserProfileForm
    readonly_fields = ('last_login', 'date_joined')
    list_display = ('username', 'first_name', 'last_name', 'last_login', 'is_active')
    list_filter = ('groups', )
    filter_horizontal = ('groups', )

    def save_model(self, request, obj, form, change):
        obj.is_staff = True
        super(UserProfileAdmin, self).save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        return super(UserAdmin, self).get_form(request, obj, **kwargs)

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super(UserProfileAdmin, self).get_fieldsets(request, obj)


admin.site.unregister(Group)
admin.site.register(UserProfile, UserProfileAdmin)
