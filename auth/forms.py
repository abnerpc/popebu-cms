# -*- coding: utf-8 -*-
from django.contrib.admin.forms import AdminAuthenticationForm
from captcha.fields import ReCaptchaField
from auth.models import *
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django import forms
from popebu import messages
from popebu.settings import RECAPTCHA_PUBLIC_KEY, RECAPTCHA_PRIVATE_KEY


class AdminAuthenticationForm(AdminAuthenticationForm):
    captcha = ReCaptchaField(
        public_key=RECAPTCHA_PUBLIC_KEY,
        private_key=RECAPTCHA_PRIVATE_KEY,
        required=False
    )

    def __init__(self, *args, **kwargs):
        super(AdminAuthenticationForm, self).__init__(*args, **kwargs)
        if not 'attempt' in self.request.session or self.request.session['attempt'] < 2:
            self.fields.pop('captcha')

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        message = _(messages.USER_LOGIN_ERROR)
        params = {'username': self.username_field.verbose_name}

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            
            if self.user_cache is None:
                if not self.errors:
                    if not 'attempt' in self.request.session:
                        self.request.session['attempt'] = 0
                    self.request.session['attempt'] += 1                
                raise forms.ValidationError(message, code='invalid', params=params)
            elif not self.user_cache.is_active or not self.user_cache.is_staff:
                raise forms.ValidationError(message, code='invalid', params=params)
        self.request.session['attempt'] = 0
        return self.cleaned_data


class UserProfileForm(forms.ModelForm):
    username = forms.RegexField(
        label=_("Username"), max_length=30, regex=r"^[\w.@+-]+$",
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    email = forms.EmailField(
        label=_('Email'),
        required=True)

    class Meta:
        model = UserProfile

    def clean_email(self):
        email = self.cleaned_data['email']
        instance = getattr(self, 'instance', None)

        result = UserProfile.objects.filter(email=email)
        if instance:
            result = result.exclude(id=instance.id)
        if result.count():
            raise forms.ValidationError(_(messages.UNIQUE_FIELD))
        return email
