.. Popebu CMS documentation master file, created by
   sphinx-quickstart on Wed Mar  5 15:17:30 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem vindo ao Popebu CMS!
======================================

.. toctree::
   :maxdepth: 2
   
   index
   
Popebu é um CMS opensource baseado no Django_.
 
.. _Django: https://www.djangoproject.com/

Sinta-se a vontade para fazer um fork ou relatar um incidente no repositório do projeto.

Requisitos
------

- Python 2.7+
- Pip
- Mysql
- mysql-connector-python
- Virtualenv (opcional)

Instalação
------

1. Depois de baixar o código do projeto e descompactá-lo em uma pasta, é necessário instalar as bibliotecas de dependência do projeto. Caso tenha optado por utilizar o virtualenv, crie e ative o seu ambiente antes da instalação das bibliotecas.
::
    $ pip install -r popebu/conf/requirements/requirements.txt
2. Depois de instalar as bibliotecas, inicie o servidor da aplicação. Caso esteja desenvolvendo um projeto local, o servidor de desenvolvimento do Django pode ser utilizado.
::
    $ python manage.py runserver
 
3. Em um primeiro acesso, é exibida a página de setup do Popebu onde você deve informar alguns dados básicos para a configuração inicial do projeto.
4. Depois de concluir a configuração do projeto com sucesso, reinicie o servidor da aplicação.


Documentação
------
.. toctree::
   :maxdepth: 2
   
   iniciando