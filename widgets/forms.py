# -*- coding: utf-8 -*-
from django import forms
from widgets.models import Contato


class ContatoForm(forms.ModelForm):
    class Meta:
        model = Contato
        fields = ('nome', 'email', 'categoria', 'assunto', 'mensagem')

