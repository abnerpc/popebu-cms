# -*- coding: utf-8 -*-
from django.contrib import admin
from widgets.models import Contato, Destaque
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.contrib.contenttypes.models import ContentType
from django.template.response import TemplateResponse


class ContatoAdmin(admin.ModelAdmin):
    fields = (
        'nome', 
        'email', 
        ('categoria', 'dt_envio',),
        'assunto',
        'mensagem',)
    list_display = ('dt_envio', 'categoria', 'assunto', 'email')
    
    def get_readonly_fields(self, request, obj=None):
        return ('nome', 'email', 'categoria', 'dt_envio', 'assunto', 'mensagem',)
    
    def has_add_permission(self, request):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Contato, ContatoAdmin)
admin.site.register(Destaque)