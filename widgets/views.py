from django.shortcuts import render_to_response, render
from administracao.models import Postagem
from django.http import Http404
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from widgets.forms import ContatoForm
from django.views.generic.edit import FormView


def get_post(request, id):
    try:
        obj = Postagem.objects.get(id=id)
    except Postagem.DoesNotExist:
        raise Http404
    return render_to_response(
        'blog/single.html',
        {'obj': obj},
        context_instance=RequestContext(request))

@login_required(login_url='/admin/')
def get_preview(request, id):
    return get_post(request, id)


def get_posts(request):
    postagens = Postagem.objects.filter(rascunho=False).order_by('-dt_criacao')
    paginator = Paginator(postagens, 6)
    page = request.GET.get('page')
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)
    return render_to_response(
        'blog/index.html',
        {'result': result},
        context_instance=RequestContext(request))
    
    
def get_posts_por_usuario(request, id):
    postagens = Postagem.objects.filter(rascunho=False, usuario__id=id).order_by('-dt_criacao')
    paginator = Paginator(postagens, 6)
    page = request.GET.get('page')
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)
    return render_to_response(
        'blog/index.html',
        {'result': result},
        context_instance=RequestContext(request))
    
    
def get_posts_por_categoria(request, id):
    postagens = Postagem.objects.filter(rascunho=False, categoria__id=id).order_by('-dt_criacao')
    paginator = Paginator(postagens, 6)
    page = request.GET.get('page')
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)
    return render_to_response(
        'blog/index.html',
        {'result': result},
        context_instance=RequestContext(request))


class ContatoView(FormView):
    template_name = 'blog/contato.html'
    form_class = ContatoForm
    success_url = '/enviado/'
contato = ContatoView.as_view()
