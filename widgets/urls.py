from django.conf.urls import patterns, url

urlpatterns = patterns('widgets.views',
    url(r'^$', 'get_posts'),
    url(r'^posts/(?P<id>\d)$', 'get_post'),
    url(r'^preview/(?P<id>\d)$', 'get_preview'),
    url(r'^posts/user/(?P<id>\d)$', 'get_posts_por_usuario'),
    url(r'^posts/categoria/(?P<id>\d)$', 'get_posts_por_categoria'),
    url(r'^contato$', 'contato'),
)