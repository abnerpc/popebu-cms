#-*- coding: utf-8 -*-
from django.db import models
import datetime
from administracao.models import Postagem


class Destaque(models.Model):
    postagem = models.ForeignKey(Postagem)
    ativo = models.BooleanField('Ativo', default=False)
    imagem = models.ImageField(
        'Imagem', 
        upload_to='upload', 
        blank=True, 
        null=True,
        help_text='Caso uma imagem não seja selecionada, será usada a imagem cadastrada na postagem.')
    resumo = models.TextField(
        'Resumo', 
        max_length=255, 
        blank=True, 
        null=True,
        help_text='Caso um resumo não seja preenchido, será usado o início do conteúdo da postagem.')
    
    class Meta:
        verbose_name = 'Destaque'
        verbose_name_plural = '%ss' % verbose_name
        
    def __unicode__(self):
        return self.postagem.titulo
    
    def get_imagem(self):
        if self.imagem:
            return self.imagem
        return self.postagem.imagem
    
    def get_resumo(self):
        if self.resumo:
            return self.resumo
        return self.postagem.resumo(255)
    
    
class Contato(models.Model):
    categorias = (
        ('1', 'Ilustrações'),
        ('2', 'Parcerias'),
        ('3', 'Sites'),
        ('0', 'Outros'),
    )
    nome = models.CharField('Nome', max_length=125)
    email = models.EmailField('Email')
    assunto = models.CharField('Assunto', max_length=125)
    categoria = models.CharField('Categoria', choices=categorias, max_length=1)
    dt_envio = models.DateTimeField('Data de envio', default=datetime.datetime.now())
    mensagem = models.TextField('Mensagem', max_length=255)
    
    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = '%ss' % verbose_name
        ordering = ('-dt_envio',)
        
    def __unicode__(self):
        return self.assunto