# -*- coding: utf-8 -*-
from django.shortcuts import render
from setup.forms import SetupForm
from popebu.conf.settings.settings_base import BASE_DIR
import os 
import json
from auth.models import UserProfile
from django.contrib.sites.models import Site
from subprocess import call
from django.http import HttpResponse
import time


def setup(request):
    if request.method == 'POST':
        form = SetupForm(request.POST)
        if form.is_valid():
            # Criando arquivo de settings
            file = open(os.path.join(BASE_DIR, 'popebu', 'settings.py'), 'w+')
            file.write('from popebu.conf.settings.settings_base import *\n')
            
            data = form.cleaned_data
            file.write('DEBUG = %s\n' % data['debug'])
            file.write('ALLOWED_HOSTS = ["%s"]\n' % data['allowed_hosts'].replace('http://', '').replace('https://', '')[:-1])
            file.write('RECAPTCHA_PUBLIC_KEY = "%s"\n' % data['recaptcha_public_key'])
            file.write('RECAPTCHA_PRIVATE_KEY = "%s"\n' % data['recaptcha_private_key'])
            
            database = {}
            database['default'] = {}
            database['default']['ENGINE'] = 'django.db.backends.mysql'
            database['default']['NAME'] = data['db_nome']
            database['default']['USER'] = data['db_user']
            database['default']['PASSWORD'] = data['db_senha']
            database['default']['HOST'] = data['db_host']
            database['default']['PORT'] = data['db_porta']
            file.write('DATABASES = %s\n' % json.dumps(database))
            
            if data['solr_url']:
                haystack = {}
                haystack['default'] = {}
                haystack['default']['ENGINE'] = 'haystack.backends.solr_backend.SolrEngine'
                haystack['default']['URL'] = data['solr_url']
                file.write('HAYSTACK_CONNECTIONS = %s\n' % json.dumps(haystack))
            
            if data['memcached_url']:
                cache = {}
                cache['default'] = {}
                cache['default']['BACKEND'] = 'django.core.cache.backends.memcached.MemcachedCache'
                cache['default']['LOCATION'] = data['memcached_url']
                cache['default']['TIMEOUT'] = '600'
                file.write('CACHES = %s\n' % json.dumps(cache))
                file.write('CACHE_MIDDLEWARE_SECONDS = 600\n')
                file.write('SESSION_ENGINE = "django.contrib.sessions.backends.cache"\n')
            file.close()
            
            # Preparando banco
            call(['python', 'manage.py', 'syncdb', '--noinput'])
            call(['python', 'manage.py', 'migrate'])
            call(['python', 'manage.py', 'loaddata', os.path.join(BASE_DIR, 'popebu', 'conf', 'data', '00-grupos.json')])
                        
            command = [
                'mysql',
                '-u%s' % database['default']['USER'], 
                '-p%s' % database['default']['PASSWORD'],  
                '%s' % database['default']['NAME'], ]
            if database['default']['HOST']:
                command.append('-h%s' % database['default']['HOST'])
            if database['default']['PORT']:
                command.append('-P%s' % database['default']['PORT'])
            command.append('-e')

            # Criando usuário
            user = UserProfile(
                username=data['superuser_username'],
                email=data['superuser_email'])
            user.set_password(data['superuser_senha'])            
            call(command + ['INSERT INTO auth_userprofile (username, password, email, is_staff, is_superuser, is_active) VALUES ("%s", "%s", "%s", 1, 1, 1);' % (user.username, user.password, user.email)])
            
            # Criando site
            call(command + ['INSERT INTO django_site (name, domain) VALUES ("%s", "%s");' % (data['nome'], data['allowed_hosts'])])
            
            time.sleep(5)
            return render(request, 'popebu/confirm_setup.html')
    else:
        form = SetupForm()
        
    return render(request, 'popebu/setup.html', {'form': form})