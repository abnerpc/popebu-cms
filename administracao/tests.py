from django.test import TestCase, RequestFactory
from administracao.models import Postagem
from administracao.admin import PostagemAdmin
from django.forms.models import modelform_factory
from django.contrib.admin.sites import AdminSite
from auth.models import UserProfile
from django.core.urlresolvers import reverse
from datetime import datetime
from django.test.client import Client
    

class PostagemTest(TestCase):
    fixtures = (
        'popebu/conf/data/00-grupos.json', 
        'popebu/conf/data/01-user.json',
        'popebu/conf/data/02-sites.json',
        'popebu/conf/data/03-categorias.json',
        'popebu/conf/data/04-postagens.json',)
    
    def setUp(self):
        self.site = AdminSite()
        self.admin = PostagemAdmin(Postagem, self.site)
        
        self.factory = RequestFactory()        
        self.form = modelform_factory(Postagem)
        
        self.client = Client()
        self.client.login(username='Master', password='admin')
        
    def test_request_add(self):
        url = reverse('admin:administracao_postagem_add')
        self.request = self.factory.get(url)
        self.request.user = UserProfile.objects.get(id=1)
        self.request.POST = {
            'titulo': 'Teste', 
            'dt_criacao': datetime.now(), 
            'categoria': 1}
        response = self.client.post(url, self.request.POST)
        self.assertEqual(response.status_code, 200)
        
    def test_request_index(self):
        url = '/'
        self.request = self.factory.get(url)
        response = self.client.post(url, self.request.POST)
        self.assertEqual(response.status_code, 200)
        
    def test_request_single_post(self):
        url = '/posts/1'
        self.request = self.factory.get(url)
        response = self.client.post(url, self.request.POST)
        self.assertEqual(response.status_code, 200)
        
    def test_request_single_post_fail(self):
        url = '/posts/2'
        self.request = self.factory.get(url)
        response = self.client.post(url, self.request.POST)
        self.assertEqual(response.status_code, 404)
        
    def test_request_index_page(self):
        url = '/?page=2'
        self.request = self.factory.get(url)
        response = self.client.post(url, self.request.POST)
        self.assertEqual(response.status_code, 200)
        
    def test_admin_save_model(self):
        obj = Postagem.objects.get(id=1)
        self.request = self.factory.get('%s/%s' % (reverse('admin:administracao_postagem_add'), obj.id))
        self.request.user = UserProfile.objects.get(id=1)
        self.admin.save_model(self.request, obj, self.form, True)