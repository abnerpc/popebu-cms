# -*- coding: utf-8 -*-
from django.db import models
from auth.models import UserProfile
from ckeditor.fields import RichTextField
from django.core.urlresolvers import reverse
import datetime
from django.db.models import signals
from django.contrib.sites.management import create_default_site
from django.contrib.sites import models as site_app
from subprocess import call


class Categoria(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True, verbose_name='Categoria superior')
    nome = models.CharField('Nome', max_length=25, unique=True)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = '%ss' % verbose_name
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('widgets.views.get_posts_por_categoria', args=[self.id])


class Postagem(models.Model):
    usuario = models.ForeignKey(UserProfile)
    categoria = models.ForeignKey(Categoria)
    titulo = models.CharField('Título', max_length=50)
    conteudo = RichTextField(blank=True, null=True)
    dt_criacao = models.DateTimeField('Data de criação', default=datetime.datetime.now())
    dt_atualizacao = models.DateTimeField(
        'Última atualização', 
        blank=True, 
        null=True)
    tags = models.CharField('Tags', max_length=255, blank=True, null=True)
    imagem = models.ImageField('Imagem', upload_to='upload', blank=True, null=True)
    rascunho = models.BooleanField('Rascunho', default=True)

    class Meta:
        verbose_name = 'Postagem'
        verbose_name_plural = '%sns' % verbose_name[:-1]
        ordering = ('-dt_criacao', '-dt_atualizacao', 'titulo', )

    def __unicode__(self):
        return self.titulo
    
    def conteudo_texto(self):
        import html2text
        return html2text.html2text(self.conteudo)

    def resumo(self, caracteres=15):
        return self.conteudo_texto()[:caracteres]

    def get_absolute_url(self):
        return reverse('widgets.views.get_post', args=(self.id,))

    def get_preview(self):
        return reverse('widgets.views.get_preview', args=(self.id,))
    
    def tags_split(self):
        return self.tags.split(',')


def update_index(sender, **kwargs):
    try:
        call(['python', 'manage.py', 'update_index', '-v', '0'])
    except Exception as e:
        pass

signals.post_save.connect(update_index, sender=Postagem)

signals.post_syncdb.disconnect(create_default_site, sender=site_app)