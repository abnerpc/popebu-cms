import datetime
from haystack import indexes
from administracao.models import Postagem


class PostagemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    conteudo = indexes.CharField(model_attr='conteudo')
    tags = indexes.CharField(model_attr='tags')
    
    def get_model(self):
        return Postagem
    
    def index_queryset(self, using=None):
        return self.get_model().objects.filter(dt_criacao__lte=datetime.datetime.now(), rascunho=False)
    