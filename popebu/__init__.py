import os
from subprocess import call
from django.conf import settings

if not os.path.isdir(settings.CKEDITOR_UPLOAD_PATH):
    call(['mkdir', '-p', settings.CKEDITOR_UPLOAD_PATH])
