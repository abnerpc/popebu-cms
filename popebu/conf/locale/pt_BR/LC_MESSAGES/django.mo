��          |      �             !  $   @     e  
   �  0   �  S   �  k     Y   |     �  *   �       @       `  (   �     �     �  2   �  r   	  o   |  O   �     <  #   J     n        	      
                                             %(site_name)s | Password reset %(site_name)s | User account created Email fields do not match. Management Please change your password on your first login. Unable to connect on database. Are you sure that connection parameters are correct? You are receiving this email because you requested a password reset for your user account at %(site_name)s. You are receiving this email because your user account at %(site_name)s has been created. Your password: Your username, in case you have forgotten: Your username: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-01 16:38-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %(site_name)s | Reiniciar senha %(site_name)s | Conta de usuário criada Campos de email não conferem. Gerenciamento Por favor mude seu password no seu primeiro login. Não é possível conectar ao banco de dados. Você tem certeza de que os parâmetros de conexão estão corretos? Você está recebendo este email porque requisitou reiniciar a senha de sua conta de usuário em %(site_name)s. Você está recbendo este email porque a sua conta em %(site_name)s foi criada. Seu password: Seu usuário, caso tenha esquecido: Seu username: 