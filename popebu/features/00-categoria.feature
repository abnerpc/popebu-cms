# language: pt-br
Funcionalidade: Cadastro de categoria
    Para criar uma categoria
    Enquanto Master
    
    Cenario: Criar categoria com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Categorias'
        E clico no botao adicionar
        E preencho 'nome' com 'XPTO1'
        E seleciono 'parent' com 'Teste'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was added successfully'
    
    Cenario: Criar categoria com sucesso (somente campos obrigatorios)
        Dado que estou na pagina inicial
        Clico no link 'Categorias'
        E clico no botao adicionar
        E preencho 'nome' com 'XPTO2'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was added successfully'
    
    Cenario: Criar categoria sem sucesso (sem campos obrigatorios)
        Dado que estou na pagina inicial
        Clico no link 'Categorias'
        E clico no botao adicionar
        E clico no botao 'Save'
        Entao vejo a mensagem 'field is required'
    
    Cenario: Criar categoria sem sucesso (nome de categoria ja existe)
        Dado que estou na pagina inicial
        Clico no link 'Categorias'
        E clico no botao adicionar
        E preencho 'nome' com 'XPTO1'
        E clico no botao 'Save'
        Entao vejo a mensagem 'already exists'
        
    Cenario: Alterar categoria com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Categorias'
        E clico no link 'XPTO1'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was changed successfully'
        
    Cenario: Excluir categoria com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Categorias'
        E clico no link 'XPTO1'
        E clico no botao apagar
        E confirmo a exclusao
        Entao vejo a mensagem 'was deleted successfully'
