# language: pt-br
Funcionalidade: Cadastro de postagem
    Para criar uma postagem
    Enquanto Master

    Cenario: Criar postagem com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no botao adicionar
        E seleciono o arquivo 'perfil.jpg' no campo 'imagem'
        E preencho 'titulo' com 'XPTO1'
        E seleciono 'categoria' com 'Teste'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was added successfully'

    Cenario: Criar postagem com sucesso (somente campos obrigatórios)
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no botao adicionar
        E preencho 'titulo' com 'XPTO2'
        E desmarco 'rascunho'
        E seleciono 'categoria' com 'Teste'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was added successfully'

    Cenario: Criar postagem sem sucesso (campos obrigatórios)
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no botao adicionar
        E clico no botao 'Save'
        Entao vejo a mensagem 'field is required'

    Cenario: Criar postagem sem sucesso (sem título)
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no botao adicionar
        E seleciono 'categoria' com 'Teste'
        E clico no botao 'Save'
        Entao vejo a mensagem 'field is required'

    Cenario: Criar postagem sem sucesso (sem categoria)
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no botao adicionar
        E preencho 'titulo' com 'Teste'
        E clico no botao 'Save'
        Entao vejo a mensagem 'field is required'
        
    Cenario: Alterar postagem com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no link 'XPTO1'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was changed successfully'
        
    Cenario: Excluir postagem com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Postagens'
        E clico no link 'XPTO1'
        E clico no botao apagar
        E confirmo a exclusao
        Entao vejo a mensagem 'was deleted successfully'