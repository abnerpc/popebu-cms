# -*- coding: utf-8 -*-
from lettuce import *
from splinter.browser import Browser
import time
from popebu.settings import TEST_UPLOAD_DIR, BASE_DIR
import os
from django.core.management import call_command
from splinter.exceptions import ElementDoesNotExist

@before.all
def set_browser():
    call_command('flush')
    call_command('syncdb')
    try:
        call_command('migrate')
    except:
        pass
    call_command('loaddata', os.path.join(BASE_DIR, 'popebu', 'conf', 'data', '00-grupos.json'))
    call_command('loaddata', os.path.join(BASE_DIR, 'popebu', 'conf', 'data', '01-user.json'))
    call_command('loaddata', os.path.join(BASE_DIR, 'popebu', 'conf', 'data', '02-sites.json'))
    call_command('loaddata', os.path.join(BASE_DIR, 'popebu', 'conf', 'data', '03-categorias.json'))
    call_command('loaddata', os.path.join(BASE_DIR, 'popebu', 'conf', 'data', '04-postagens.json'))
    
    world.browser = Browser()
    
@after.all
def quit_browser(after):
    world.browser.quit()

@before.each_feature
def setup_feature(feature):
    world.browser.cookies.delete()
    world.browser.visit('http://localhost:7000/admin/')
    world.browser.fill('username', 'Master')
    world.browser.fill('password', 'admin')
    world.browser.find_by_value('Log in').first.click()
    
@after.each_feature
def cleanup_feature(feature):
    try:
        world.browser.find_by_css('[href="/admin/logout/"]').first.click()
    except ElementDoesNotExist:
        pass
    
@after.each_step
def wait(step):
    time.sleep(float(0.5))

@step('Dado que estou na pagina inicial')
def ir_pagina_inicial(step):
    world.browser.visit('http://localhost:7000/admin/')
    
@step('preencho \'([^\']*)\' com \'([^\']*)\'')
def preencher_campo(step, campo, valor):
    world.browser.fill(campo, valor)

@step('clico no botao \'([^\']*)\'')
def clicar_botao(step, valor):
    world.browser.find_by_value(valor).first.click()
    
@step('Clico no link \'([^\']*)\'')
def clicar_link(step, valor):
    for link in world.browser.find_link_by_text(valor):
        if link.visible:
            link.click()
            break
    
@step('clico no botao adicionar')
def clicar_add(step):
    world.browser.find_by_css('[href*="/add/"]').first.click()
    
@step('seleciono \'([^\']*)\' com \'([^\']*)\'')
def selecionar_campo(step, campo, valor):
    for item in world.browser.find_by_css('select[name="%s"] > option' % campo):
        if item.text == valor:
            item.click()
            break

@step('seleciono o arquivo \'([^\']*)\' no campo \'([^\']*)\'')
def selecionar_arquivo(step, arquivo, campo):
    world.browser.attach_file(campo, os.path.join(TEST_UPLOAD_DIR, arquivo))
    
@step('vejo a mensagem \'([^\']*)\'')
def procurar_mensagem(step, valor):
    assert world.browser.is_text_present(valor)
    
@step('E desmarco \'([^\']*)\'')
def desmacar_campo(step, campo):
    world.browser.uncheck(campo)
    
@step('clico no \'([^\']*)\' registro')
def clicar_registro(step, index):
    world.browser.find_by_css('tr[class*="row"] > th > a')[int(index)-1].click()
    
@step('clico no botao apagar')
def clicar_apagar(step):
    world.browser.find_by_css('[href*="/delete/"]').first.click()
    
@step('confirmo a exclusao')
def clicar_confirmar(step):
    world.browser.find_by_value("Yes, I'm sure").first.click()
    
@step('Debug')
def debugar(step):
    import pdb; pdb.set_trace()
    
@step('Logout')
def deslogar(step):
    world.browser.find_by_css('[href="/admin/logout/"]').first.click()
    
@step('vejo o campo \'([^\']*)\'')
def procurar_campo(step, campo):
    assert world.browser.is_element_present_by_name(campo)
    
@step('Login')
def logar(step):
    world.browser.cookies.delete()
    world.browser.visit('http://localhost:7000/admin/')
    world.browser.fill('username', 'Master')
    world.browser.fill('password', 'admin')
    world.browser.find_by_value('Log in').first.click()